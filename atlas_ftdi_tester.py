"""
    Python code for testing Atlas Sensors which is connected with FTDI adaptors.
    @ author: Wester de Weerdt
    @ email: wester.de.weerdt@dutchmail.com
    @ date: 07/08/2016
"""

from pylibftdi.device import Device
from pylibftdi.driver import FtdiError
from pylibftdi import Driver
import os
import time


class AtlasFTDITester(Device):

    def __init__(self, sn):
        Device.__init__(self, mode='t', device_id=sn)

    def read_line(self):
        lsl = len(os.linesep)
        line_buffer = []
        try:
            start_time = time.time()
            while True:
                next_char = self.read(1)
                if next_char == "\r":
                    break
                line_buffer.append(next_char)
                if len(line_buffer) >= lsl and line_buffer[-lsl:] == list(os.linesep):
                    break
                if time.time() - start_time > 1.0:  # timeout
                    line_buffer = ''
                    break
            return ''.join(line_buffer)

        except FtdiError:
            return None

    def send_cmd(self, cmd):
        print "Send command: ", cmd
        buf = cmd + "\r"
        try:
            self.write(buf)
            return True
        except FtdiError:
            return False


def get_ftdi_device_list():
    """
    return a list of lines, each a colon-separated
    vendor:product:serial summary of detected devices
    """
    dev_list = []

    for device in Driver().list_devices():
        # list_devices returns bytes rather than strings
        dev_info = map(lambda x: x.decode('latin1'), device)
        # device must always be this triple
        vendor, product, serial = dev_info
        dev_list.append(serial)
    return dev_list

if __name__ == '__main__':

    devices = None
    cnt_all = 0
    result = []
    cnt_success = 0
    cnt_fail = 0

    print "USB TESTER\n1. 'L' = List all devices\n2. 'T' = Test all devices"
    print "3. Ctrl-c exit"

    while True:
        cmd = raw_input("Enter command: ")
        cnt_success = 0
        cnt_fail = 0

        if cmd in ['l', 'L']:
            print "Discovered devices' serial numbers:"
            devices = get_ftdi_device_list()
            cnt_all = len(devices)
            for i in range(cnt_all):
                print i + 1, ": ", devices[i]
        else:
            if devices is None:
                print "Please discover devices with 'list' command"
            else:
                if cmd in ['t', 'T']:
                    for i in range(cnt_all):
                        print "Number: ", i+1, "  Serial : ", devices[i]
                        try:
                            dev = AtlasFTDITester(devices[i])

                            if not dev.send_cmd("I"):
                                print "Failed to send command.\n"
                                cnt_fail += 1
                                continue
                            else:
                                re = dev.read_line()
                                if re[0] != "?":
                                    print "Response Error.\n"
                                    cnt_fail += 1
                                else:
                                    val_list = re.split(',')
                                    print "Sensor type: ", val_list[1], "    Firmware version: ", val_list[2]
                                    cnt_success += 1
                                    print ""
                        except FtdiError:
                            print "Failed to initialize device.\n"
                            cnt_fail += 1
                        except IndexError as e:
                            print "Error, ", e
                            cnt_fail += 1

                    if cnt_fail == 0:
                        print "All ", cnt_all, " PASSED"
                    else:
                        print cnt_success, " passed"
                        print cnt_fail, " failed"

                elif cmd.lower()[:5] == 'poll,':
                    try:
                        num = int(cmd.split(',')[1])
                        interval = int(cmd.split(',')[2])
                        print "ALL: ", cnt_all, "Num: ", num
                        if num > cnt_all or num < 1:
                            print "Please input correct number"
                        else:
                            try:
                                dev = AtlasFTDITester(devices[num - 1])
                                while True:
                                    s_time = time.time()
                                    dev.send_cmd("I")
                                    print "Response: ", dev.read_line()
                                    time.sleep(interval + s_time - time.time())
                            except KeyboardInterrupt:  # catches the ctrl-c command, which breaks the loop above
                                print("Continuous polling stopped")
                    except ValueError:
                        print "Please insert correct number of the board."

                else:
                    print "Unknown input."

        print ""

